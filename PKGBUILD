# Maintainer: tioguda <guda.flavio@gmail.com>

pkgbase=droidcam
kernel=4.19
_extramodules=extramodules-${kernel}-MANJARO
_linuxprefix=linux${kernel/./}
pkgname=${_linuxprefix}-${pkgbase}
_pkgver=1.8.0
pkgver=1.8.0_4.19.218_1
pkgrel=1
pkgdesc='Driver for Droidcam'
arch=('x86_64')
url="https://github.com/aramg/${pkgbase}"
license=('GPL')
makedepends=("${_linuxprefix}" "${_linuxprefix}-headers")
provides=("${pkgbase}-modules=${kernel}")
conflicts=("${pkgbase}-dkms" "${pkgbase}-dkms-git")
groups=("$_linuxprefix-extramodules")
install="${pkgbase}.install"
source=("${pkgbase}-${_pkgver}.zip::${url}/archive/v${_pkgver}.zip")
sha512sums=('937626a4b122739224053b0a2ded9a52b2d7d203614705c06a3205169575f354b88d3721607b347ca9e565689fdd8ff417e26cfde8ed8e69c5f814b0c8a13fc4')

pkgver() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    printf '%s' "${_pkgver}_${_ver/-/_}"
}

prepare() {
    cd "${srcdir}/${pkgbase}-${_pkgver}"
}

build() {
    _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"
    cd "${srcdir}/${pkgbase}-${_pkgver}"

    make -C /usr/lib/modules/$_kernver/build \
    M="${srcdir}/${pkgbase}-${_pkgver}/v4l2loopback" \
    modules
}

package() {
    _ver=$(pacman -Q $_linuxprefix | cut -d " " -f 2)
    depends=("${_linuxprefix}=${_ver}" "${pkgbase}=${_pkgver}")

    cd "${srcdir}/${pkgbase}-${_pkgver}"

    install -Dm755 "v4l2loopback/v4l2loopback-dc.ko" "${pkgdir}/usr/lib/modules/${_extramodules}/v4l2loopback-dc.ko"
    find "${pkgdir}" -name '*.ko' -exec xz {} +

    # set the kernel we've built for inside the install script
    sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${pkgbase}.install"
}

